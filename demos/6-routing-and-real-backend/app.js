angular.module('myApp', ['projects']);

angular.module('myApp').config(function($routeProvider) {
	$routeProvider
		.when('/', {
			controller:'ListCtrl as listCtrl',
			templateUrl:'list.html'
		})
		.when('/new', {
			controller:'CreateCtrl as formCtrl',
			templateUrl:'form.html'
		})
		.otherwise({
			redirectTo:'/'
		});
});
