angular.module('projects').controller('ListCtrl', function($scope, projectsRepository) {
	projectsRepository.fetchAll().then(function(projects) {
		$scope.projects = projects;
	});
});

angular.module('projects').controller('CreateCtrl', function($scope, $location, projectsRepository) {
	this.save = function() {
		projectsRepository.create($scope.project).then(function() {
			$location.path('/');
		}).catch(function() {
			console.log('unhandled server error', arguments);
		});
	};
});

angular.module('projects').controller('EditCtrl', function($scope, $location, $routeParams, projectsRepository) {

	var projectId = $routeParams.projectId;

	projectsRepository.fetchOne(projectId).then(function(project) {
		$scope.project = project;
	}).catch(function() {
		console.log('unhandled server error', arguments);
	});

	this.destroy = function() {
		projectsRepository.remove($scope.project).then(function() {
			$location.path('/');
		}).catch(function() {
			console.log('unhandled server error', arguments);
		});
	};

	this.save = function() {
		projectsRepository.update($scope.project).then(function() {
			$location.path('/');
		}).catch(function() {
			console.log('unhandled server error', arguments);
		});
	};
});