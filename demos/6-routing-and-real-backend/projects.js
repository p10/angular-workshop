angular.module('projects', ['firebase', 'ngRoute']);

angular.module('projects').value('fbURL', 'https://1389116825015.firebaseio.com/javascript_projects');

angular.module('projects').factory('projectsRepository', function($firebase, $q, fbURL) {

	var fire =  $firebase(new Firebase(fbURL)).$asArray();
	var service = {};

	service.fetchAll = function() {
		var deffered = $q.defer();
		fire.$loaded().then(function() {
			deffered.resolve(fire);
		}).catch(function() {
			deffered.reject.apply(deffered, arguments);
		});
		return deffered.promise;
	};

	service.fetchOne = function($id) {
		var deffered = $q.defer();
		fire.$loaded().then(function() {
			var index = fire.$indexFor($id);
			deffered.resolve(fire[index]);
		}).catch(function() {
			deffered.reject.apply(deffered, arguments);
		});
		return deffered.promise;
	};

	service.create = function(project) {
		return fire.$add(project);
	};

	service.update = function(project) {
		return fire.$save(project);
	};

	service.remove = function(project) {
		return fire.$remove(project);
	};

	return service;
});



