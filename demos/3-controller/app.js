angular.module('myApp', []);

angular.module('myApp').controller('myCtrl', function($scope) {
	$scope.model = 'default model value';

	this.controllerMethod = function() {
		// new way
		// requires "Ctrl as alias" syntax in template
		$scope.model = '';
	};

	$scope.anotherControllerMethod = function() {
		// old school way
	};

});