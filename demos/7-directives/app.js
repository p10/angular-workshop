angular.module('myApp', ['projects', 'projectDirectives', 'ui.bootstrap']);

angular.module('myApp').controller('MainCtrl', function($scope, projectsRepository, $modal) {
	var formats = ['h:mm:ss a', 'H:mm:ss'];
  $scope.format = formats[0];

  projectsRepository.fetchAll().then(function(projects) {
    $scope.projects = projects;
  });

  this.changeDateFormat = function() {
    alert('implement me using $modal');
  };

});

angular.module('myApp').controller('ModalCtrl', function($scope, $modalInstance, formats, format) {
  // modal controller
});

angular.module('myApp').directive('myCurrentTime', function($interval, dateFilter) {
  return {
    link: function(scope, element, attrs) {
      var format, timeoutId;

      function updateTime() {
        element.text(dateFilter(new Date(), format));
      }

      scope.$watch(attrs.myCurrentTime, function(value) {
        format = value;
        updateTime();
      });

      element.on('$destroy', function() {
        $interval.cancel(timeoutId);
      });

      // start the UI update process; save the timeoutId for canceling
      timeoutId = $interval(updateTime, 1000);
    }
  };
});