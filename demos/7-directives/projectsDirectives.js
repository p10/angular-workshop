angular.module('projectDirectives', ['projects']);

angular.module('projectDirectives').directive('projectInfo', function() {
  return {
    restrict: 'E',
    scope: {
      project: '='
    },
    templateUrl: 'projectInfo.html'
  };
});

angular.module('projectDirectives').directive('projectList', function() {
	return {
		restrict: 'E',
		scope: {
			title: '@',
			projects: '='
		},
		templateUrl: 'projectList.html'
	};
});