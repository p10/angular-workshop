angular.module('myApp', ['anotherModule']);

angular.module('myApp').controller('FirstCtrl', function($scope, messageTransmitter) {
	$scope.model = messageTransmitter;
});

angular.module('myApp').controller('SecondCtrl', function($scope, messageTransmitter) {
	$scope.model = messageTransmitter;
});


angular.module('anotherModule', []);
angular.module('anotherModule').factory('messageTransmitter', function() {
	var service = {};
	service.message = "model value from service";
	return service;
});