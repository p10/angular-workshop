angular.module('myApp', []);

angular.module('myApp').controller('myCtrl', function($scope, articleRepository) {

	articleRepository.fetchArticles().then(function(articles) {
		$scope.articles = articles;
	});

});

angular.module('myApp').factory('articleRepository', function($http) {

	var service = {};

	service.fetchArticles = function() {
		return $http.get('articles.json').then(function(httpResponse) {
			return httpResponse.data;
		});
	};

	return service;
});